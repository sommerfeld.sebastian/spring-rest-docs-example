FROM openjdk:11
LABEL maintainer="sebastian@sommerfeld.io"
COPY target/spring-rest-docs-example.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]
