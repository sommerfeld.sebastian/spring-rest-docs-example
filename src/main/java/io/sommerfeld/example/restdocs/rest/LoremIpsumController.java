package io.sommerfeld.example.restdocs.rest;

import java.util.Collections;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoremIpsumController {

	@RequestMapping(path = "/lorem", method = RequestMethod.GET)
	public Map<String, Object> greeting() {
		return Collections.singletonMap("message", "Lorem ipsum dolor sit amet, consetetur sadipscing");
	}
}