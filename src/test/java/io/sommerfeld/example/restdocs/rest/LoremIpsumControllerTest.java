package io.sommerfeld.example.restdocs.rest;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;

@AutoConfigureRestDocs
@WebMvcTest(LoremIpsumController.class)
public class LoremIpsumControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void shouldReturnHelloWorld() throws Exception {
		this.mockMvc.perform(get("/lorem"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string(containsString("Lorem ipsum dolor sit amet, consetetur sadipscing")))
				.andDo(document("LoremIpsumController", responseFields(fieldWithPath("message").description("A field description goes here."))));
	}
}