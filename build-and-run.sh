#!/bin/bash
# @file build-and-run.sh
# @brief Build and run the Spring Boot Demo Application.
#
# @description The script triggers a Maven build using ``mvn package`` and runs the Spring Boot Demo Application. The
# target folder is cleared first. After the Maven build the artifact is built into a Docker image and started in a#
# container.
#
# The container is not running in the background and gets removed once it shuts down.
#
# | What                                                                      | Port | Protocol |
# | ------------------------------------------------------------------------- | ---- | -------- |
# | Spring Boot Demo Application (Docker container using locally built image) | 8080 | http     |
#
# ==== Arguments
#
# The script does not accept any parameters.

echo -e "$LOG_INFO JAVA_HOME for this script = $JAVA_HOME"
echo -e "$LOG_INFO Maven Installation" && which mvn
echo -e "$LOG_INFO Maven Version" && mvn --version

echo -e "$LOG_INFO Lint Dockerfile"
docker run --rm -i hadolint/hadolint < Dockerfile

echo -e "$LOG_INFO Clean"
mvn clean

echo -e "$LOG_INFO Test java application"
mvn test

echo -e "$LOG_INFO Build java application"
mvn package

# ----- DEPRECATED start --------------
#echo -e "$LOG_INFO"
#echo -e "$LOG_INFO RUN"
#(
#  cd target || exit
#  "$JAVA_HOME/bin/java" -jar spring-rest-docs-example
#)
# ----- DEPRECATED end ----------------

echo -e "$LOG_INFO Build docker image"
docker build -t kobol/spring-rest-docs-example:latest .

echo -e "$LOG_INFO Start application in container"
docker run --rm -p 8080:8080 kobol/spring-rest-docs-example:latest
