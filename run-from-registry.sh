#!/bin/bash
# @file run-from-registry.sh
# @brief Run the Spring Boot Demo Application using the previously built Docker image from Dockerhub.
#
# @description The script runs the Spring Boot Demo Application using the previously built
## link:https://hub.docker.com/r/sommerfeldio/spring-rest-docs-example[Docker image from Dockerhub].
#
# The container is not running in the background and gets removed once it shuts down.
#
# | What                                                                                     | Port | Protocol |
# | ---------------------------------------------------------------------------------------- | ---- | -------- |
# | Spring Boot Demo Application (Image = ``sommerfeldio/spring-rest-docs-example:nightly``) | 8080 | http     |
#
# ==== Arguments
#
# The script does not accept any parameters.

echo -e "$LOG_INFO Start application in container"
docker run --rm -p 8080:8080 sommerfeldio/spring-rest-docs-example:nightly
